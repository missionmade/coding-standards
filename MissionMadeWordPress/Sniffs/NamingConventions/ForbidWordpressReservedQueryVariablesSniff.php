<?php

declare(strict_types=1);

namespace MissionMade\MissionMadeWordPress\Sniffs\NamingConventions;

use PHP_CodeSniffer\Files\File;
use PHP_CodeSniffer\Sniffs\Sniff;

final class ForbidWordpressReservedQueryVariablesSniff implements Sniff
{
    private $forbiddenKeys = [
        'attachment',
        'attachment_id',
        'author',
        'author_name',
        'calendar',
        'cat',
        'category_name',
        'category__and',
        'category__in',
        'category__not_in',
        'comments_per_page',
        'comments_popup',
        'cpage',
        'day',
        'debug',
        'error',
        'exact',
        'feed',
        'hour',
        'link',
        'minute',
        'monthnum',
        'more',
        'name',
        'nav_menu',
        'nopaging',
        'offset',
        'order',
        'orderby',
        'p',
        'page',
        'paged',
        'pagename',
        'page_id',
        'pb',
        'perm',
        'post',
        'posts',
        'posts_per_archive_page',
        'posts_per_page',
        'post_format',
        'post_mime_type',
        'post_status',
        'post_type',
        'preview',
        'robots',
        's',
        'search',
        'second',
        'sentence',
        'showposts',
        'static',
        'subpost',
        'subpost_id',
        'tag',
        'tag_id',
        'tag_slug__and',
        'tag_slug__in',
        'tag__and',
        'tag__in',
        'tag__not_in',
        'taxonomy',
        'tb',
        'term',
        'type',
        'w',
        'withcomments',
        'withoutcomments',
        'year',
    ];

    public function register()
    {
        return array(T_VARIABLE);
    }

    public function process(File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();
        $variableName = $tokens[$stackPtr]['content'];

        if ($variableName === '$_GET' || $variableName === '$_POST') {
            $nextToken = $phpcsFile->findNext(T_CONSTANT_ENCAPSED_STRING, $stackPtr + 1);
            if ($nextToken !== false && in_array(trim($tokens[$nextToken]['content'], "'"), $this->forbiddenKeys)) {
                $error = 'Forbidden key "%s" used in %s';
                $data = array(
                    $tokens[$nextToken]['content'],
                    $variableName,
                );
                $phpcsFile->addError($error, $nextToken, self::class, $data);
            }
        }
    }
}
